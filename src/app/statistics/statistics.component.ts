import { Component, OnInit } from '@angular/core';
import {PositionStat} from "../shared/model/position-stat";
import {StatisticsHttpService} from "../shared/service/statistics-http-service";
import {AgeGroupStat} from "../shared/model/age-group-stat";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  positionStats: PositionStat[] = [];
  ageGroupStats: AgeGroupStat[] = [];
  posDisplayedColumns = ['position', 'count', 'age'];
  ageDisplayedColumns = ['age', 'count'];

  constructor(
    private statisticsService: StatisticsHttpService
  ) { }

  ngOnInit(): void {
    this.loadPositionStat();
    this.loadAgeGroupStat();
  }

  loadPositionStat() {
    this.statisticsService.getPositionStat()
      .subscribe( data => {
        this.positionStats = data;
        console.log('Position statistics loaded:', this.positionStats);
      })
  }

  loadAgeGroupStat() {
    this.statisticsService.getAgeGroupStat()
      .subscribe( data => {
        this.ageGroupStats = data;
        console.log('Age group statistics loaded: ', this.ageGroupStats);
      })
  }

}
