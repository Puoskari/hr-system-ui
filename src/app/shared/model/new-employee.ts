export class NewEmployee{
  constructor(
    public username: string,
    public age: number,
    public position: string
  ) {
  }
}
