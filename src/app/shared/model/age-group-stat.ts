export class AgeGroupStat {
  constructor(
    public age: number,
    public count: string
  ) {
  }
}
