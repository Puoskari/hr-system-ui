export class PositionStat{
  constructor(
    public position: string,
    public count: number,
    public age: number
  ) {
  }
}
