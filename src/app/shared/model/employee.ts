export class Employee{
  constructor(
    public dbId: number,
    public username: string,
    public age: number,
    public position: string
  ) {
  }
}
