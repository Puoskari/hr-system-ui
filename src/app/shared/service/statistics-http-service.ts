import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {PositionStat} from "../model/position-stat";
import {AgeGroupStat} from "../model/age-group-stat";

@Injectable()
export class StatisticsHttpService {
  private static statistics = "http://localhost:8080/statistics";

  constructor(
    private http: HttpClient
  ) {
  }

  getPositionStat() {
    return this.http.get<PositionStat[]>(StatisticsHttpService.statistics +  '/position');
  }

  getAgeGroupStat() {
    return this.http.get<AgeGroupStat[]>(StatisticsHttpService.statistics + '/age-group');
  }

}
