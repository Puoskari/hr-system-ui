import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Employee} from "../model/employee";
import {NewEmployee} from "../model/new-employee";

@Injectable()
export class EmployeeHttpService {
  private static employee = "http://localhost:8080/employee";

  constructor(
    private http: HttpClient
  ) {
  }

  getAll() {
    return this.http.get<Employee[]>(EmployeeHttpService.employee+ '/all');
  }

  saveEmployee(employee: NewEmployee) {
    return this.http.post(EmployeeHttpService.employee, employee);
  }

  deleteEmployee(id: number) {
    console.log('Delete user: ', id);
    return this.http.get(EmployeeHttpService.employee + '/delete' + '?id=' + id);
  }
}
