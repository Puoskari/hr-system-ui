import { Component, OnInit } from '@angular/core';
import {Form, FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {EmployeeHttpService} from "../../shared/service/employee-http-service";
import {Employee} from "../../shared/model/employee";
import {NewEmployee} from "../../shared/model/new-employee";
import {Observable} from "rxjs";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-add-edit-employee',
  templateUrl: './add-edit-employee.component.html',
  styleUrls: ['./add-edit-employee.component.css']
})
export class AddEditEmployeeComponent implements OnInit {
  employeeForm!: FormGroup;

  constructor(
    private employeeService: EmployeeHttpService,
    private dialogRef: MatDialogRef<AddEditEmployeeComponent>
  ) {}

  ngOnInit(): void {
    this.employeeForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'age': new FormControl(null, [Validators.required, Validators.pattern("^[0-9]*$")]),
      'position': new FormControl(null, Validators.required)
    })
  }

  onSubmit() {
    let submit = new NewEmployee(
      this.employeeForm.value.username,
      this.employeeForm.value.age,
      this.employeeForm.value.position);
    console.log('Submitting employee: ', submit);
    this.close(this.employeeService.saveEmployee(submit));
  }

  private close(obs: Observable<Object>) {
    obs.subscribe( _ => this.dialogRef.close());
  }

}
