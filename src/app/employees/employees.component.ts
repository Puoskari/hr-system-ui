import { Component, OnInit } from '@angular/core';
import {EmployeeHttpService} from "../shared/service/employee-http-service";
import {Employee} from "../shared/model/employee";
import {MatDialog} from "@angular/material/dialog";
import {AddEditEmployeeComponent} from "./add-edit-employee/add-edit-employee.component";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees: Employee[] = [];
  displayedColumns = ['username', 'age', 'position', 'delete']

  constructor(
    public employeeService: EmployeeHttpService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.employeeService.getAll()
      .subscribe(data => {
        this.employees = data;
      })
  }

  onAdd() {
    this.dialog.open(AddEditEmployeeComponent).afterClosed().subscribe( _ => this.loadUsers());
  }

  onDelete(id: number) {
    console.log('Submit to delete user with id: ', id);
    this.employeeService.deleteEmployee(id)
      .subscribe(_ => {
        this.loadUsers();
        console.log('Deletion done');
      });
  }

}
